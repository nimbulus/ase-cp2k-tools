import ase
from ase.io import read, write
from ase.visualize import view
import numpy as np
import copy
"""working logic: Because of the different heights of the carbon and other adsorbed species,
simply invoking center() will change the position of the slabs, which may not be desired, as the way
dipole correction works is to place capacitor plates next to the slabs. Therefore, we desire a fixed
position for all the images. This can be conveniently achieved by centering the slab alone, and then moving 
the carbon to the position it needs to go"""

"""option2: hardcode N0 to position 2.43773184,1.60120071,8.93091260"""

PBC = [19.473530671280287,20.744,30.0]

structure = read("slab.xyz")
ganonly = ase.Atoms([i for i in structure if i.symbol in ["Ga","N"] ])

structure.cell = PBC
ganonly.cell = PBC
ganonly.pbc = True
structure.pbc = True
pos0 = ganonly[0].position
pos0 = copy.copy(pos0)
ganonly.center()
pos1 = ganonly[0].position
print(pos1)
print(pos0)
trvec = pos1 - pos0

for atom in structure:
    atom.position = atom.position+trvec
    
write("slab-recentered.xyz",structure)