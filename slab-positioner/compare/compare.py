#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 12 17:35:56 2022

@author: randon
"""


from os import listdir
from os.path import isfile, join
import ase
import os
onlyfiles = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f)) and "xyz" in f]

#handle our own KINDS
bodge = read("test.xyz")
anums = ase.data.atomic_numbers.copy()
anums.update({
        "Gaplus":anums['Ga'],
        "Nminus":anums['N']})

def refix(symbols):
    if isinstance(symbols, str):
        symbols = refix(symbols)
    numbers = []
    for s in symbols:
        if isinstance(s, str):
            numbers.append(anums[s])
        else:
            numbers.append(int(s))
    return numbers
ase.data.atomic_numbers = anums

ase.symbols.symbols2numbers = refix
structures = [ase.io.read(u) for u in onlyfiles if ".xyz" in u]
for i,s in enumerate(structures):
    print(s[0].position,[s])