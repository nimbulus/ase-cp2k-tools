#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  4 02:39:00 2021

@author: randon
"""
import itertools
import numpy as np
class Tree(object):
    def __init__(self, data, children=None, parent=None):
        self.data = data
        self.children = children or []
        self.parent = parent

    def add_child(self, data):
        new_child = Tree(data, parent=self)
        self.children.append(new_child)
        return new_child

    def is_root(self):
        return self.parent is None

    def is_leaf(self):
        return not self.children

    def __str__(self):
        if self.is_leaf():
            return str(self.data)
        return '{data} [{children}]'.format(data=self.data, children=', '.join(map(str, self.children)))
N=7
broken = [3,2,2] #some hypothetical sum to N

A0 = Tree(data=np.arange(N),parent=None)
childNodes = {"level":["Tree'd items"],0:[A0]}
for level, num in enumerate(broken):
    childNodes.update({level+1:[]})
    for iterableOption in childNodes[level]:
        print(iterableOption.data)
        for combination in itertools.combinations(iterableOption.data, num):
            An = Tree(combination,parent=iterableOption)
            childNodes[level+1].append(An)
        