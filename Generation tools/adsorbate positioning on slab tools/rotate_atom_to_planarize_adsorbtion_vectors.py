#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 19:56:29 2021

@author: randon
"""

import catkit
from catkit import Gratoms
from catkit.build import molecule
from catkit.gen.surface import SlabGenerator
from catkit.gen.adsorption import AdsorptionSites
from catkit.gen.adsorption import Builder
from ase.build import bulk
from ase.visualize import view
from scipy.optimize import minimize, basinhopping
import numpy as np
import pysmiles
import generator
from openbabel import pybel
import openbabel as ob
import networkx as nx
import itertools
import os
import ase

#rotate as defined by ASE only rotates whole molecules
#To do this, define a point in the molecule to separate it into 2
#use the graph data embedded to split the atom into 2; at the bond before the second, problematic node containing X. 
def norm(u):
    return u/np.sqrt((u.dot(u)))
def idealVec(mol):
    #returns an ideal vector based on the vector of the first C-X bond
    vecs = []
    print(mol)
    for edge in mol.graph.edges:
        for atom in edge:
            if mol[atom].symbol=="X":
                #I'm a adsorption bond!
                vecs.append(edge)
    print(vecs)
    if len(vecs)>0:
        C = 0
        if mol[vecs[0][0]].symbol=="X":
            a = vecs[0][1]
            b = vecs[0][0]
            C = b
        else:
            a = vecs[0][0]
            C = a
            b = vecs[0][1]
        #always returns the vector from X to C
        return mol[a].position - mol[b].position ,vecs, C


def nakedness(mol,vec=None,hinge=None):
    #vec is a vector, hinge is an integer indexing the carbon the X is attached to
    if type(vec)==type(None):
        vec,edge,hinge = idealVec(mol)
        vec = norm(vec)        
    #returns the greatest any other bond overlaps the adsorption vector
    overlap = 0
    offending_atom = 0
    for atom in mol:
        if atom.symbol!="X":
            ctoatom = mol[hinge].position - atom.position
            olap = ctoatom.dot(vec)
            if olap>overlap:
                overlap = olap
                offending_atom = atom
    return overlap, offending_atom


def rotateNode(molecule,deg,vec):
    #take the molecule, rotate the node then minimize
    #emt potential for X is not available - pretend it's F
    xindicies = [i.index for i in molecule if i.symbol=="X"]
    hingeIndices = []
    _vec,edges,_h = idealVec(molecule)
    for e in edges:
        if molecule[e[0]].symbol=='X':
            hingeIndices.append(e[1])
        else:
            hingeIndices.append(e[0])
    immediateNeighbours = {}
    for hinge in hingeIndices:
        immediateNeighbours.update({hinge:[]})
        for edge in molecule.graph.edges:
            if hinge in edge:
               immediateNeighbours[hinge].append(set(edge) - set([hinge]) )
    #once you have the neighbours, figure out which neighbour corresponds to the backbone, and which one is "free to rotate".
    #then rotate along an axis - the axis may be ideally the one q
    for hinge in hingeIndices:
        pass #we could rotate, but the optimization takes too long!
    
def planarize(mol,bondlength=1.2):
    #tries to place the adsorption vectors into the same plane
    def allVecs(mol):
        vec, vecs, hinge = idealVec(mol)
        vecpos = []
        for v in vecs:
            if mol[v[0]].symbol=="X":
                a = v[1]
                b = v[0]
            else:
                a = v[0]
                b = v[1]
    
            vecpos.append(mol[a].position - mol[b].position)
        return vecpos
    #generate lots of conformers
    def splitSdf(sdffile,ofiles):
        with open(sdffile,'r') as f:
            text = f.read()
            text = text.split('$$$$')
            text = text[:len(text)-1]
            for idx,a in enumerate(text):
                with open(ofiles+str(idx)+".sdf",'w') as of:
                    if a[0]=='\n':
                        a = a[1:]
                    of.write(a)
        return len(text)
    xindicies = [i.index for i in mol if i.symbol=="X"]
    mol = mol.copy()
    for atom in mol:
        if atom.symbol=="X":
            atom.symbol="F"
    mol.write("./obscratch/obinput.xyz")
    res = os.system("obabel ./obscratch/obinput.xyz -O ./obscratch/obout/oboutput.sdf --confab -conf 300 --writeconformers")
    if res!=0:
        print("error!")
        os.system("rm ./obscratch/*")
        os.system("rm ./obscratch/obout/*")
        os.system("rm ./obscratch/obout/individuals/*")
    else:
        #print('succ')
        conformers = []
        nakedlist = []
        n = splitSdf("./obscratch/obout/oboutput.sdf","./obscratch/obout/individuals/")
        print(n)
        for i in range(n):
            print(i)
            c = ase.io.read("./obscratch/obout/individuals/"+str(i)+".sdf")
            c = Gratoms(c)
            for index in xindicies:
                c[index].symbol="X"
            c._graph = mol.graph
            
            #conformers.append(c)
            print(c.graph.edges)
            print(c.graph.nodes)
            print(xindicies)
            _v, edges, _h = idealVec(c)
            vecs = []
            hinges = []
            for v in edges:
                if mol[v[0]].symbol=="X":
                    a = v[1]
                    hinges.append(v[1])
                    b = v[0]
                else:
                    a = v[0]
                    hinges.append(v[0])
                    b = v[1]
                vecs.append(c[a].position - c[b].position)
            minOlap = 10
            if len(vecs)>1:
                for v in vecs[1:]:
                    v0 = norm(vecs[0])
                    if v.dot(v0)<minOlap:
                        minOlap = v.dot(v0)
            leastNaked = 0
            for idx, v in enumerate(vecs):
                naked = nakedness(c,norm(v),hinges[idx])
                if naked[0] > leastNaked:
                    leastNaked = naked[0]
                    
            if leastNaked > bondlength or minOlap < 0.1:
                print('rejected',leastNaked,minOlap)
                print(naked,norm(v),hinges[idx])
                #view(c)
                #break
            else:
                conformers.append(c)
                nakedlist.append(leastNaked)
            #print(i)
        #done with this conformer set
        os.system("rm ./obscratch/*.xyz")
        os.system("rm ./obscratch/obout/*.sdf")
        os.system("rm ./obscratch/obout/individuals/*.sdf")

    return conformers, nakedlist