#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 27 21:03:34 2021

@author: randon
"""
import catkit
from catkit import Gratoms
from catkit.build import molecule
from catkit.gen.surface import SlabGenerator
from catkit.gen.adsorption import AdsorptionSites
from catkit.gen.adsorption import Builder
from ase.build import bulk
from ase.visualize import view
from scipy.optimize import minimize, basinhopping, dual_annealing, differential_evolution
import numpy as np
import pysmiles
from openbabel import pybel
import openbabel as ob
import networkx as nx
import itertools
from numba import jit
from ase.atom import Atom
def norm(u):
    return np.sqrt((u).dot(u))


saturation = {"H":1,"O":4,"S":4,"Se":4,"N":4,"P":4,"As":4,"C":4,"Si":4} #maximum reasonable adsorbate valencies
NeutralSaturation = {"H":1,"O":2,"S":2,"Se":2,"N":3,"P":3,"As":3,"C":4,"Si":4}
def generateAnagramsWithAnchorPoints(molString,MolAnchorPoints=None,satDict=saturation):
    """Generates a list of anagrams for the given string, and then
    tags the potential adsorbate points with dummy element X. The
    adsorption vector is generated to be the most likely, given the saturation
    of the node in question."""
    anagrams = molecule(molString)
    for mol in anagrams:
        """This step finds likely anchor points and tags them with X"""
        #there is a get_unsaturated_nodes() but it's buggy... \'keyerror valence: line 140 radicals=data['valence']\
        #we will roll our own
        for atom in mol:
            #print(atom.symbol)cen
            if atom.symbol in saturation:
                desSat = satDict[atom.symbol] #assuming that O, etc, N, etc, will always be able to adsorb!
                actualSat = len(mol.get_neighbor_symbols(atom.index))
                degUnsat = desSat - actualSat
                if degUnsat > 0:
                    """atom is undersaturated, tag as adsorp candidate"""
                    #X's index will be just len(mol)
                    mol.append("X")
                    mol.graph.add_node(len(mol)-1)
                    mol.graph.add_edge(atom.index,len(mol)-1)
                    #print(mol.graph.edges)
                    mol = catkit.gen.molecules.get_3D_positions(mol)
        
            else:
                print("Pass a value for the saturation of the atom ",atom.symbol," as {'X':Num}")
                return
    return anagrams
def generateAnchoredMoleculeFromSmiles(molecule,satDict=saturation,bondLength=1.8):
    ptable = {'0':'X','1': 'H', '2': 'He', '3': 'Li', '4': 'Be', '5': 'B', '6': 'C', '7': 'N', '8': 'O', '9': 'F', '10': 'Ne', '11': 'Na', '12': 'Mg', '13': 'Al', '14': 'Si', '15': 'P', '16': 'S', '17': 'Cl', '18': 'Ar', '19': 'K', '20': 'Ca', '21': 'Sc', '22': 'Ti', '23': 'V', '24': 'Cr', '25': 'Mn', '26': 'Fe', '27': 'Co', '28': 'Ni', '29': 'Cu', '30': 'Zn', '31': 'Ga', '32': 'Ge', '33': 'As', '34': 'Se', '35': 'Br', '36': 'Kr', '37': 'Rb', '38': 'Sr', '39': 'Y', '40': 'Zr', '41': 'Nb', '42': 'Mo', '43': 'Tc', '44': 'Ru', '45': 'Rh', '46': 'Pd', '47': 'Ag', '48': 'Cd', '49': 'In', '50': 'Sn', '51': 'Sb', '52': 'Te', '53': 'I', '54': 'Xe', '55': 'Cs', '56': 'Ba', '57': 'La', '58': 'Ce', '59': 'Pr', '60': 'Nd', '61': 'Pm', '62': 'Sm', '63': 'Eu', '64': 'Gd', '65': 'Tb', '66': 'Dy', '67': 'Ho', '68': 'Er', '69': 'Tm', '70': 'Yb', '71': 'Lu', '72': 'Hf', '73': 'Ta', '74': 'W', '75': 'Re', '76': 'Os', '77': 'Ir', '78': 'Pt', '79': 'Au', '80': 'Hg', '81': 'Tl', '82': 'Pb', '83': 'Bi', '84': 'Po', '85': 'At', '86': 'Rn', '87': 'Fr', '88': 'Ra', '89': 'Ac', '90': 'Th', '91': 'Pa', '92': 'U', '93': 'Np', '94': 'Pu', '95': 'Am', '96': 'Cm', '97': 'Bk', '98': 'Cf', '99': 'Es', '100': 'Fm', '101': 'Md', '102': 'No', '103': 'Lr', '104': 'Rf', '105': 'Db', '106': 'Sg', '107': 'Bh', '108': 'Hs', '109': 'Mt', '110': 'Ds', '111': 'Rg', '112': 'Cn', '113': 'Nh', '114': 'Fl', '115': 'Mc', '116': 'Lv', '117': 'Ts', '118': 'Og'}
    """Generates a list of anagrams from a SMILES string and then tags the undersaturated
    nodes as potential anchors using the dummy element X"""
    mymol = pybel.readstring("smi",molecule)
    gen3d = ob.OBOp.FindType("gen3D")
    gen3d.Do(mymol.OBMol,"--best")
    symbols = [ptable[str(atom.atomicnum)] for i, atom in enumerate(mymol.atoms)]
    grmol = Gratoms()
    for atom in symbols:
        grmol.append(atom)
    mymolgraph = nx.Graph()
    for x, atom in enumerate(mymol.atoms):
        mymolgraph.add_node(x)
        for y, atom in enumerate(mymol.atoms): #careful, the atoms in OBMol are indexed at +1
            if mymol.OBMol.GetBond(x+1,y+1)!=None:
                #bond exists, 
                mymolgraph.add_node(y)
                mymolgraph.add_edge(x,y)
    grmol._graph = mymolgraph
    for idx, atm in enumerate(mymol.atoms):
        grmol[idx].position = atm.coords
    for atom in grmol:
        desSat = satDict[atom.symbol]
        X = ob.OBAtom()
        X.SetAtomicNum(0)
        degUnsat = 0
        if atom.symbol in saturation:
            desSat = satDict[atom.symbol] #assuming that O, etc, N, etc, will always be able to adsorb!
            actualSat = len(grmol.get_neighbor_symbols(atom.index))
            degUnsat = desSat - actualSat
            if degUnsat > 0:
                mymol.OBMol.AddAtom(X)
                mymol.OBMol.AddBond(atom.index+1,len(mymol.atoms),1)
                #bond = mymol.OBMol.GetBond(atom.index+1,len(mymol.atoms))
                #sbond.SetLength(2)
    
    #======tagging in pybel done, regenerate gratoms
    gen3d.Do(mymol.OBMol,"--best") #regenerate 3D
    mymolgraph = nx.Graph() #regenerate graph
    grmol = Gratoms()
    symbols = [ptable[str(atom.atomicnum)] for i, atom in enumerate(mymol.atoms)]
    for atom in symbols:
        grmol.append(atom)

    for x, atom in enumerate(mymol.atoms):
        mymolgraph.add_node(x)
        
        for y, atom in enumerate(mymol.atoms): #careful, the atoms in OBMol are indexed at +1
            if mymol.OBMol.GetBond(x+1,y+1)!=None:
                #bond exists, 
                mymolgraph.add_node(y)
                mymolgraph.add_edge(x,y)
    grmol._graph = mymolgraph
    for idx, atm in enumerate(mymol.atoms):
        grmol[idx].position = atm.coords
        if grmol[idx].symbol in saturation:
            desSat = satDict[grmol[idx].symbol] #assuming that O, etc, N, etc, will always be able to adsorb!
            actualSat = len(grmol.get_neighbor_symbols(idx)) - len([x for x in grmol.get_neighbor_symbols(idx) if x=="X"])
            degUnsat = desSat - actualSat
            grmol.graph.nodes[idx].update({"unsaturation":degUnsat})

        if grmol[idx].symbol=="X": #by default the 3d embedder of pybel puts X-C bond too close. Change it. 
            neighbor = [i for i in grmol.graph.neighbors(idx)][0] #there should only be one anyways
            mvect = grmol[idx].position - grmol[neighbor].position 
            grmol[idx].position = grmol[neighbor].position + (mvect/norm(mvect))*bondLength
    return grmol
def generateSmilesAnagramsFromElementalFormula(formula):
    """
    

    Parameters
    ----------
    formula : A string that is an elemental formula. Note: You cannot just say "CH4", you need to specify
    "C1H4" - single appearanches of an element will be (at worst) quietly ignored! Something like "CH3CH3" will not be
    recognized properly! Only elemental formulas allowed!

    Returns
    -------
    A list of SMILES strings for each possible anagram of the formula.
    
    ----
    This was an unbelievable function. There are several helper functions embedded, they are also documented.
    """
    class Tree(object):
        def __init__(self, data, children=None, parent=None,name=None):
            self.data = data
            self.children = children or []
            self.parent = parent
            self.name = name
        def add_child(self, data):
            new_child = Tree(data, parent=self)
            self.children.append(new_child)
            return new_child
    
        def is_root(self):
            return self.parent is None
    
        def is_leaf(self):
            return not self.children
    
        def __str__(self):
            if self.is_leaf():
                return str(self.data)
            return '{data} [{children}]'.format(data=self.data, children=', '.join(map(str, self.children)))

    def graphsOnNNodes(N):
        """WARNING! this increases EXPONENTIALLY with N!!
        N -  a natural number
        returns: a list of unique graphs on N nodes
        """
        nodes = np.arange(N)
        graphs = []
        allbonds = [c for c in itertools.combinations(nodes,2)]
        minBonds = N-1
        for numBonds in range(minBonds,N+1):
            isomers = [i for i in itertools.combinations(allbonds,numBonds)]
            for isomer in isomers:
                mygraph = nx.Graph()
                mygraph.add_nodes_from(nodes)
                mygraph.add_edges_from(isomer)
                isunique = True
                for graph in graphs:
                    if len(graphs)>0:
                        if nx.is_isomorphic(graph,mygraph):
                            isunique = False
                            break
                if isunique and nx.connected.is_connected(mygraph):
                    graphs.append(mygraph)
        return graphs
    def getNHeavyAtoms(formula):
        """
        Parses an elemental formula for the number of hydrogens and each atom
        ----------
        formula : An elemental function.

        Returns
        -------
        N : Total number of non-Hydrogen atoms
        NH : total number of hydrogens.
        retdict: a dict {"C":Ncarbons,"N":NNitrogens,etc}
        """
        nums = "1234567890"
        atoms = list(saturation.keys()) #this should be improved honestly...
        atoms.remove("H")
        atoms.sort(reverse=True)
        N = 0
        retdict = {}
        for atom in atoms:
            if formula.find(atom)!=-1:
                
                idx = formula.find(atom) + len(atom)-1
                goforit = False
                try:
                    #print(atom,formula[idx],formula[idx+1])
                    if formula[idx+1] not in nums:
                        if formula[idx]+formula[idx+1]==atom:
                            print("i am unique",formula[idx],formula[idx+1])
                            goforit = True
                except IndexError as ie:
                    #print(formula[idx],"terminates")
                    goforit = True
                i = 1
                nstr = "0"
                while (idx+i)<len(formula) and formula[idx+i] in nums:
                    nstr = nstr+formula[idx+i]
                    i = i+1
                if int(nstr)==0:
                    nstr = "1"
                    
                print("found",atom,nstr)
                retdict.update({atom:int(nstr)})
                N = N+int(nstr)
                print(atom+str(int(nstr)))
                formula = formula.replace(atom+str(int(nstr)),"")
                print(formula)
        if len(formula)>1: #
            NH = int(formula[1:])
        else:
            NH= 0
        return N, NH, retdict
    def assignNodes(root,idents):
        """
        Parameters
        ----------
        root : a root node, ie Tree(data=np.array(N),parent=None)
            This contains the np.array(N) that represents all the nodes available to our graph
        idents : a dict of {'element':Number of elements} 
            obtained from getNHeavyAtoms, it describes the number of each element that it identified from parsing the
            initial elemental formula. ie CH4O2 will get the idents = {'C':1,'O',2}

        important data structures
        -------
        Childnodes : a dict, with each key being the "level" of a tree. Every item in a level itself is an element of
            a tree, and retains parent information to identify which nodes are which.
            IE:
                let [0,1,2,3,4,5,6] be all the possible nodes in a graph I want to decorate, and I have 3 carbons, 2 nitrogens
                and 2 oxygens. Every way to decorate a node with an element would look like the following:
                    level 1: [nodes for carbon], which leaves  behind unused nodes that can go to nitrogen and oxygen.
                    there are N choose NumCarbon branches for the carbons. Then to determine where to put the nitrogens, I consider
                    N-NumCarbon choose NumNitrogen positions on the remaining unused nodes. These unused nodes are only relevant
                    when we know which ones have already been used by carbon, so we keep track of that information by setting
                    the parent to the list of nodes used by Carbon. This process repeats for every element I need to consider.
            It looks finally like {0:[nodes],
                                   1:[(nodes for element 1)..(every combination of nodes for element 1) ],
                                   2:[(nodes for element 2)..(every combination of nodes for element 2, considering that
                                                              some have already been used by element 1,)],
                                   3:[same process, ad infinitum]}
            every element in each level has a property, name, which is the element that the nodes correspond to. 
            DESCRIPTION.
            Returns:
            ----------
            list of SMILES strings corresponding to possible structures. 
        """
        childNodes = {0:[A0]} #{dict of {level:[list of tree'd items]}}
        for level, element in enumerate(idents):
            #print(element)
            childNodes.update({level+1:[]})
            for iterableOption in childNodes[level]:
                #print(iterableOption.data)
                if level>0:
                    AlreadyEnumerated = np.array([])
                    cursor = iterableOption
                    while cursor.parent is not None:
                        AlreadyEnumerated = np.append(AlreadyEnumerated,np.array(cursor.data).copy())
                        cursor = cursor.parent
                    notEnumerated = np.setdiff1d(A0.data,AlreadyEnumerated)
                    #print("Already enumeratd",AlreadyEnumerated,"itoption parent",iterableOption.data)
                else:
                    notEnumerated = A0.data.copy()
                #print("Not Enumerated:",notEnumerated,"level",level)
                for combination in itertools.combinations(notEnumerated, idents[element]):
                    An = Tree(combination,parent=iterableOption,name=element)
                    childNodes[level+1].append(An)
        for node in childNodes:
            for litem in childNodes[node]:
                heritage = ""
                cursor = litem
                while cursor.parent is not None:
                    heritage = heritage+">"+str(cursor.parent)
                    cursor = cursor.parent
                #print(litem,heritage,litem.name)
        return childNodes
    def checknode(n,m):
        """simple function to determine if two things are equal. Used for nx.is_isomporphic when checking nodes"""
        if n!=m:
            return False
        else:
            return True
    #========Helper functions done=====================================================
    backboneNum,NumH,idents = getNHeavyAtoms(formula)
    #print("Got:",backboneNum,NumH,idents)
    print("generating graphs on",backboneNum,"nodes")
    graphs = graphsOnNNodes(backboneNum)
    A0 = Tree(data=np.arange(backboneNum))
    nodes = assignNodes(A0,idents)
    BackBoneAnagrams = []
    for graph in graphs:
        #print("I expect ",len(nodes[len(nodes)-1]))
        for finalNode in nodes[len(nodes)-1]:
            curgraph = graph.copy()
            #{NODEID: {'element': 'C', 'charge': 0, 'aromatic': False, 'hcount': NH}
            cursor = finalNode
            while cursor.parent is not None:
                element = cursor.name
                for nodeID in cursor.data:
                    curgraph.nodes[nodeID].update({'element':element,'charge':0,'aromatic':False,'hcount':0})
                cursor = cursor.parent
            isAcceptable = True
            for node in curgraph:
                elem = curgraph.nodes[node]['element']
                maxSat = NeutralSaturation[elem]
                neighbors = [i for i in curgraph.neighbors(node)]
                curSat = len(neighbors)
                #print(elem,maxSat,neighbors)
                if curSat>maxSat:
                    isAcceptable = False
            for a in BackBoneAnagrams:
                if nx.is_isomorphic(a,curgraph,checknode):
                    isAcceptable = False
            if isAcceptable:
                #print(len(curgraph))
                BackBoneAnagrams.append(curgraph)
            #print(element,finalNode)
    
    
    #for each anagram, decorate with hydrogens...
    anagrams = []
    hmap = itertools.combinations_with_replacement(np.arange(backboneNum), NumH)
    print("generating positions for hydrogen...")
    for anagram in BackBoneAnagrams:
        for hmapping in hmap:
            mymap = []
            for i in np.arange(backboneNum):
                mymap.append(hmapping.count(i))
            
            curgraph = anagram.copy()
            isGood = True
            for idx,hcount in enumerate(mymap):
                elem = curgraph.nodes[idx]['element']
                maxSat = NeutralSaturation[elem]
                neighbors = [i for i in curgraph.neighbors(idx)]
                curSat = len(neighbors)
                if hcount<=(maxSat-curSat):
                    curgraph.nodes[idx]['hcount']=hcount
                else:
                    #print("impossible mapping!")
                    #print(mymap,"on ",curgraph.edges)
                    isGood = False
            for accepted in anagrams:
                if nx.is_isomorphic(curgraph,accepted,checknode):
                    isGood = False
            if isGood:
                #print("got good mapping:")
                #print(mymap,"on ",curgraph.edges)
                #print(len(curgraph))
                anagrams.append(curgraph)
        hmap = itertools.combinations_with_replacement(np.arange(backboneNum), NumH)
        #fill up the hcount to the max possible for the node. Then generate a list of nodes from which to 
        #add hydrogens based on total NumHydrogen. Then try to remove those hydrogens from said nodes. If not possible,
        #skip that anagram, and move on to the next combination of possible nodes to remove hydrogen from.
        #the tool itertools.combinations_with_replacement should be useful!
    #for each graph in mygraphs, set indexes to carbons based on combinations of num carbons
    #by decorating as {NODEID: {'element': 'C', 'charge': 0, 'aromatic': False, 'hcount': 2}
    #then for each one of these, the remainder can be decorated with the other atoms...
    smiles = []
    for gram in anagrams:
        #print(len(anagrams))
        #print(len(gram))
        #print(pysmiles.write_smiles(gram))
        if pysmiles.write_smiles(gram)=='[C]':
            return gram
        smiles.append(pysmiles.write_smiles(gram))
    return smiles

def rotate_and_translate(adsorbate,slab,translate,rotate,rotcenter):
    #move the adsorbate above the CENTROID of the slab. Do this by knowing 1/2 of the cell's x,y coords.
    adsorbate = adsorbate.copy()
    #print(adsorbate.edges)
    #print(translate,rotate)
    slabCenter = np.array([slab.cell[0]/2,slab.cell[1]/2,slab.cell[2]/2]).dot((np.array([1,1,1])))+np.array([0,0,5])
    slabIdx = [i for i in range(len(slab))]
    adsIdx = [i+len(slab) for i in range(len(adsorbate))]
    for idx, axis in enumerate(['x','y','z']):
        adsorbate.rotate(rotate[idx],axis,center=rotcenter)
    rc = slab.copy()
    #print(slab.graph)
    #simply adding a Gratoms to a Gratoms breaks the edges information that we need!
    for atom in adsorbate:
        rc.append(atom)
        rc.graph.add_node(atom.index+len(slab)) #manually handle the nodes
    adsEdges = adsorbate.edges
    for edge in adsEdges:
        rc.graph.add_edge(edge[0]+len(slab),edge[1]+len(slab))
    
    for adatom in adsIdx:
        rc[adatom].position += slabCenter+translate
    #print(rc.edges)
    return rc

def loss(candidatePos,slabAtomSym,clashdistC=5,clashdistH=4.4):
    candidatePos = candidatePos.copy()
    adsites = [atom.position for atom in candidatePos if atom.symbol=="Ra"]
    #assuming that Ra is the dummy atom for the slab and X is the dummy atom for the adsorbate
    #Ideally only a few sites near the center of the slab will be taggged with Ra.
    Xpos = []
    Rapos = []
    clash = 0
    center = candidatePos.cell/2
    minAnchorDist = 0
    for atom in candidatePos:
        if atom.symbol=="X":
            Xpos.append(atom.position)
        if atom.symbol=="Ra":
            Rapos.append(atom.position)
        if atom.symbol=="C" or atom.symbol=="H": #a bit hacky. Ideally good for stuff with more interesting atoms as well..
            for otherAtom in candidatePos: #this is unfortunately a Nads*Nslab problem. IDK what would be faster?
                if otherAtom.symbol in slabAtomSym:
                    if atom.symbol=="C":
                        if norm(otherAtom.position - atom.position)<clashdistC:
                            clash = clash+ np.exp(3*(norm(otherAtom.position - atom.position)-clashdistC))
                    if atom.symbol=="H":
                        if norm(otherAtom.position - atom.position)<clashdistH:
                            clash = clash+ np.exp(3*(norm(otherAtom.position - atom.position)-clashdistH) )
        if atom.position[2]<(center[2][2]+5.5): #avoid getting stuck in slab
            clash = clash+10*(1*(center[2][2]+5.5)-atom.position[2])**2
    #loss is smaller if all the Xs and Ras are close to each other.
    #loss increases when clashes occur
    for adsAnchor in Xpos:
        myDist = []
        for slabPoint in Rapos:
            myDist.append( (8*(norm(adsAnchor - slabPoint) ) )**2 ) #is a square a better function?
        minAnchorDist = minAnchorDist+min(myDist)
    #print(clash+minAnchorDist)
    return minAnchorDist + clash

def lloss(candidatePos,slabAtomSym,clashdistC=5,clashdistH=4.4,bondlength=1.8):
    candidatePos = candidatePos.copy()
    minXAnchorDist = 0
    minCanchorDist = 0
    globalVectors = []
    vecScore = 0
    Xpos = []
    Rapos = [] #Ra/Be stands in for the point X should adsorb to
    Cpos = [] 
    Upos = [] #U/Be acts as the node C should be closest to 
    for atom in candidatePos:
        if atom.symbol=="X":
            Xpos.append(atom.position)
            neighbour = 0
            for at in candidatePos.edges(): #rotate_and_translate breaks edge information! Cannot trust it!
                #print(at)
                if atom.index in at: 
                   
                   neighbour = at[at.index(atom.index)^1]
                   #print(neighbour)
            Cpos.append(candidatePos[neighbour].position)
            globalVectors.append(candidatePos[neighbour].position - atom.position)
        if atom.symbol=="Be":
            Rapos.append(atom.position) #Ra stands in for Be which stands for the Ga position of interest
        if atom.symbol=="F":
            Upos.append(atom.position)
    closestU = [] #get the closest U for each closests Ra to X, then when optimizing for Cads, only search through the U in closest U
    for Xads in Xpos:
        myDist = []
        for idx,slabPoint in enumerate(Rapos):
            myDist.append(norm(Xads-slabPoint))
        minXAnchorDist = minXAnchorDist+(min(myDist))
        closestU.append( Rapos[myDist.index(min(myDist) ) ] + np.array([0,0,bondlength]) )
        Rapos.pop(myDist.index(min(myDist))) #ensure that each Ra (Be) can only link to at most 1 X.
    for Cads in Cpos:
        myDist = []
        for slabPoint in closestU:
            myDist.append(norm(Cads-slabPoint))
        minCanchorDist = minCanchorDist+min(myDist)
        #closestU.pop(myDist.index(min(myDist))) #ensure that each U (F) can only link to one C
    #reward the molecule for pointing it's adsorption vectors in the direction of the overall adsorption vector 
    #as defined by Be - F = -(0,0,bondlength)
    normedV = np.array([0,0,bondlength])/norm(np.array([0,0,bondlength] ) )
    for vector in globalVectors:
        #print(vector/norm(vector))
        vecScore = vecScore+ norm ( vector/norm(vector) - normedV )
    if vecScore/len(Xpos)<0.7:
        vecScore = 0 #don't waste too much time making sure it's pointed in the general direction
    #print(minCanchorDist,minXAnchorDist,vecScore)
    
    return minCanchorDist**2+minXAnchorDist**2+vecScore #What is my gradient? A 6x6 jacobian matrix...
    #loss(Rc,Rx) for R = (r1+r2+r3) where rN is the position of the Nth anchor - position of the closest corresponding anchor
    #this might be just loss(Rc,Rx)=Rc^2 + Rx^2
    #its derivative wrt to its components would just be 2Rc+ 2Rx
    #Simplify since Rc = sqrt(Rcx^2+Rcy^2+Rcz^2) - > 2*(Rcx+Rcy+Rcz)/sqrt(Rcx^2+Rcy^2+Rcz^2) and ditto for Rx.
    #Now to convert the data, but that seems gay and stupid. 

#square for the vecScore because that will convince it to rotate correctly faster
def getNeighbourAtoms(adsorbate,index):
    edges = adsorbate.graph.edges(index)
    atoms = []
    for edge in edges:
        atoms.append(adsorbate[edge[1]] )
    return atoms
def generateAdsorbsRationally(adsorbate,slab,slabAnchorAtmSymbol,specialSymbols={"Cl":2,},bondlength=1.8):
    """a tagged (using generateAnagramsWithAnchorPoints) small molecule
    slab: a slab item made with the bulk() module, and SlabGenerator from the bulk, and slab = Slabgen.get_slab(). The surface atoms should be
    set using set_surface_atoms(), manually if need be (use view(slab)) to see which atoms you want
    slabAnchorAtmSymbol: By default the program looks for every adsorption site. This is to identify which atoms specifically can bind.
    returns a nice slab with adsorbate ready to plug into your favourite QM software.
    specialSymbols: If you manually tag a surface with other "adsorption points" that you identify, they will
    be converted into internal adsorption point representations as well
    
    Takes the adsorbate, rotates it so all the adsorption vectors are pointing downwards towards the slab
    Then it moves it until the distance on the XY plane between the adsorption points of the slab and the adsvec "X"
    on the adsorbate is minimized. 
    
    right now adsorbOnUnsat will be linked to specialSymbols for 
    
    TODO: Incorporate logic that puts adsorbates with degree of unsaturation = 2 on the specialSymbol tagged positions
    whilst DegUnsat = 1 on the Ga and N.
    #partially implemented- now available for NX = 2
    Can do this by: if there are more #DUS =1 than #DUS = 2, create the polygons on the Ga and N sites.
    Otherwise, do the vice versa
    If #DUS =1 == #DUS = 2 then treat both together as a large polygon. 
    #TODO:
    delete polygons that are identical except by displacement... For N>3, maybe this can be done using the cross pdt of the first
    
    2. ensure that the C-X vectors are truly unobstructed and not going to put any part of a molecule into the slab
    
    """
    slab = slab.copy()
    retslab = slab.copy()*(6,4,1)
    slabSites = AdsorptionSites(slab)
    slabSymbols = slab.symbols.species()
    connectorAtm = slabSites.get_topology()
    adsites = slabSites.get_coordinates()
    connectivity = slabSites.get_connectivity()
    slabHeight = max([i.position[2] for i in slab if i.symbol in slabAnchorAtmSymbol])
    for idx, site in enumerate(adsites):
        if connectivity[idx]==1:
            if slab[connectorAtm[idx][0]].symbol in slabAnchorAtmSymbol and slab[connectorAtm[idx][0]].position[2]==slabHeight:
                slab.append("Be")
                slab[len(slab)-1].position = site + np.array([0,0,0])
                slab.append("F")
                slab[len(slab)-1].position = site + np.array([0,0,bondlength])  
                
    slabPrimitive = slab.copy()
    slab = slab*(6,4,1)
    CXvectors = []
    for atom in adsorbate:
        if atom.symbol=="X":
            carbonIdx = list(adsorbate.graph.edges(atom.index))[0][1] #EdgeDataView([(X.index,neighbour.index)])
            CXvector = adsorbate[carbonIdx].position - atom.position
            CXvectors.append(CXvector)
    averageCXvector = np.array([0.0,0.0,0.0])
    for vec in CXvectors:
        averageCXvector+=vec
    averageCXvector = averageCXvector/len(CXvectors)
    adsorbate.rotate(averageCXvector,np.array([0.0,0.0,1.0]),center="COP")
    #move the adsorbate to (0,0,0)
    adpos = [x.position for x in adsorbate]
    adscom = np.array([0,0,0])
    for atpos in adpos:
        adscom = adscom+atpos
    adscom = adscom/len(adpos)
    adsorbate.translate(np.array([-adscom[0],-adscom[1],-adscom[2]]))
    print(averageCXvector)
    """At this point we have the CX vectors pointing into the slab
    We now need to move and rotate the adsorbate to minimize the closest-neighbour distances between X
    and the tagged adsorb positions, Be.
    To do this, we find the shape of the adsorbate by constructing a convex hull of the XY positions of
    the X atoms.
    Then, taking a 2x2 supercell of the slab, we construct a list of potential convex hulls from subsets of
    size len([x for x in adsorbate if x.symbol=="X"]); thus if the convex hull of the Xs on the adsorbate form
    a triangle, we search all the possible triangles on the supercell, keeping tab of which atoms form that supercell
    Then, we find the convex hull of the slab that closest resembles that of the adsorbate.
    
    Step 1: create a convex hull from the Xs and the slab ads points
    Step 2: re-order the convex hulls to be clockwise (as needed by the polygon clipping algorithm)
    Step 3:
        bestOverlaps, rotations = []
        for hull in slabhulls:
            bestRotations = []
            for rotatedAdsorbateHull,degree in zip(rotatedAdsorbateHulls,rotationDegree):
                bestRotation.append(hull.overlap(rotatedAdsorbateHull)/area(AdsorbedHull) )
                rotationDegree.append(degree)
            bestRotation = max(bestRotations)
            bestOverlaps.append(bestRotation)
            rotations.append(rotationDegree[bestRotations.index(bestRotation)])
            
        return max(bestOverlaps), rotations[bestOverlaps.index(max(bestOverlaps))]
        
        returning the best overlapping hull, and its rotation degree
        
    step 4: move and rotate the adsorbate so it hovers over the best overlapping hull and rotated into the best 
    conformation.
        
    """
    from shapely import affinity
    from shapely.geometry import Polygon
    from scipy.spatial import ConvexHull
    def removeDifferingByOrderOnly(iterable):
        uniquePolygons = []
        uniquePolygonsCentered = []
        for polygon in iterable:
            centroid = np.array([0,0])
            for point in polygon:
                centroid = centroid+np.array(point)
            centroid = centroid/len(polygon)
            temp = []
            centeredTemp = []
            for point in polygon:
                temp.append(tuple(point))
                centeredTemp.append(tuple(np.round_(np.array(point) - np.array(centroid),2 )))
                
            if set(centeredTemp) in uniquePolygonsCentered:
                #print(centeredTemp,"in",uniquePolygonsCentered)
                pass
            else:
                uniquePolygons.append(set(temp))
                uniquePolygonsCentered.append(set(centeredTemp))
                #print(uniquePolygonsCentered)
        return uniquePolygons
    #def checkPolygonsAreNotTranslations(polygonlist):
    #    uniquePolygons = []
    #    for polygon in polygonlist:
            #a polygon is considered equivalent if 
    def angle(vect):
        refvec = np.array([1,0,0]) #in 3d so the cross pdt works
        vect = np.array([vect[0],vect[1],0])
        look = np.array([0,0,1])
        sign = np.array(np.sign(np.cross(vect, refvec).dot(look)))
        sign[sign == 0] = 1 #if sign was 0, it was colinear
        return np.arccos(np.clip(np.dot(vect, refvec), -1.0, 1.0))*57.2958*sign #deg
    def sort_ccw(points):
        #takes a list of points and sorts them out counterclockwise by sorting the angle between the point and the centroid
        #and a reference vector
        utilpgon = Polygon(points)
        centroid = np.array(utilpgon.centroid)
        vectors = [p - centroid for p in points]
        uvectors = [v/np.linalg.norm(v) for v in vectors]
        angles = []
        class tagVect:
            angle = 0
            idx = 0
            def __lt__(self, other):
                
                return self.angle > other.angle
        def angle(vect):
            refvec = np.array([1,0,0]) #in 3d so the cross pdt works
            vect = np.array([vect[0],vect[1],0])
            look = np.array([0,0,1])
            sign = np.array(np.sign(np.cross(vect, refvec).dot(look)))
            sign[sign == 0] = 1 #if sign was 0, it was colinear
            return np.arccos(np.clip(np.dot(vect, refvec), -1.0, 1.0))*57.2958*sign #deg

        for idx,vector in enumerate(uvectors):
            taggedVector = tagVect()
            taggedVector.angle = angle(vector)
            taggedVector.idx = idx
            
            angles.append(taggedVector)
        angles.sort()
        pointret = []
        for angle in angles:
            pointret.append(points[angle.idx])
        return pointret
    Xpos = [x.position for x in adsorbate if x.symbol=="X"]
    xlist = [] # xtype
    unsatClassCount = {1:0,2:0,3:0}
    class xtype:
        unsaturation=0
        index = 0
        def __init__(self,unsat,idx):
            self.unsaturation = unsat
            self.index = idx
    for x in [x for x in adsorbate if x.symbol=="X"]:
        
        carb = getNeighbourAtoms(adsorbate,x.index) [0]
        myUnsat = carb.index
        myUnsat = adsorbate.graph.nodes[carb.index]['unsaturation']
        myX = xtype(myUnsat,x.index)
        unsatClassCount[myUnsat]+=1
        xlist.append(myX)
    ###################################
    #Various helper functions done
    ###################################
    if len(Xpos)>2: #no need to bother if we have less than 3 points while 7 points is too many
        XposInXYPlane = [x[:2] for x in Xpos]
        XposInXYPlane = sort_ccw(XposInXYPlane)
        #convert to clockwise 
        adsorbateHull = ConvexHull(XposInXYPlane)
        adsorbatePolygon = Polygon(adsorbateHull.points)
        adscentroid = np.array(adsorbatePolygon.centroid)
        
        adsorbatePolygon = affinity.translate(adsorbatePolygon,-adscentroid[0],-adscentroid[1])
        
        
        #center it
        rotations = []
        for degree in range(0,360,5):
            x = affinity.rotate(adsorbatePolygon,degree,'center')
            rotations.append(x)
        
        testSlab = slabPrimitive*(2,2,1)
        #a 2x2 square will suffice to get the possible shapes without being too expensive
        #There will be a max of 4 identical shapes for n=3 or n=4 and 2 for n>4
        
        npoints = len(adsorbateHull.points)
        if npoints>6:
            print("Error too many points in hull! Cannot do 16C7 points!")
            return
        candidateAdsorbSites = [Be.position[:2] for Be in testSlab if Be.symbol=="Be"]
        #Get all possible n-gons
        #computes all nChooseN combinations internally so this is slow. Probably fix this later...
            
        sites = []
        unsats={0:[(0,0)],1:candidateAdsorbSites,2:[],3:[],4:[]} #0 stands for adsorb me anywhere
        if(len(specialSymbols.keys())>0):
            for key in specialSymbols.keys():
                unsatSites = [Be.position[:2] for Be in testSlab if Be.symbol==key]
                unsats.update({specialSymbols[key]:unsatSites})
        #TODO: finish off this logic for more polygons
        #check what kind of adsorption are we doing. We will simply try to adsorb the "most common" degree of unsaturation, and
        #say nuts to the rest
        unsatDeg = 0
        highestYet = 0 
        for key in unsatClassCount.keys():
            if unsatClassCount[key]>highestYet:
                highestYet = unsatClassCount[key]
                unsatDeg=key
        sites = unsats[unsatDeg]
        polygonPoints = list(itertools.permutations(sites,npoints))
        #gets a list of *unique* polygons
        uniquePolygons = removeDifferingByOrderOnly(polygonPoints)
        #remove the polygons that were different only by translation. This can be done by 
        #checking the points are not similar in position
        
        #uniquePolygons = checkPolygonsAreNotTranslations(uniquePolygons)
        class polygonTranslation:
            polygon = None
            transvect = np.array([0.0,0.0])
        centeredPolygons = []
        #move all the polygons to the center
        for polypoints in uniquePolygons:
            polygon = polygonTranslation()
            polygon.polygon = Polygon(list(polypoints))
            polygon.transvect = np.array(polygon.polygon.centroid.coords)
            #move to [0,0]
            polygon.polygon = affinity.translate(polygon.polygon,-polygon.transvect[0][0],-polygon.transvect[0][1])
            centeredPolygons.append(polygon)
        #check through the polygons for which one overlaps our adsorbate the most
        class Solution:
            rotation = 0
            transvect = np.array([0.0,0.0])
            intersectArea = 0 #nb: this actually checks if the intersectpolygon is much bigger than
            #the adsorbate polygon; if it is, it will set this number to -1
            def __lt__(self,other):
                return self.intersectArea < other.intersectArea
        solutions = []
        #bodge = np.array([1.711, 1.322]) #bug in the code for where the centroid is due to a slab offset??
        for polygon in centeredPolygons:
            intersectAreas = []
            for deg,rotation in enumerate(rotations):
                deg = deg*5
                try:
                    intersectionPolygon = rotation.intersection(polygon.polygon.buffer(0))
                    
                    intersectArea = intersectionPolygon.area
                    if intersectArea>rotation.area*1.1:
                        intersectArea=-1
                    intersectAreas.append(intersectArea)
                except Exception as te:
                    print("error: ",te)
                    intarea = input("intersectArea: (-1 to skip) ")
                    intarea = float(intarea)
            #get the max intersectArea, and put it into the solutions
            maxIntersect = max(intersectAreas)
            maxIntersectAngle = intersectAreas.index(max(intersectAreas))*5
            polygonSolution = Solution()
            polygonSolution.rotation = maxIntersectAngle
            polygonSolution.intersectArea=maxIntersect
            polygonSolution.transvect = polygon.transvect
            solutions.append(polygonSolution)
            
        solutions.sort()
        if len(solutions)==0:
            print("hmn no solution??",unsatDeg,unsatClassCount,max(unsatClassCount))
            view(adsorbate)
            #fakesoln = Solution()
            #solutions.append(fakesoln)
        #bestsoln = max(solutions)
        #print(max(solutions).rotation, max(solutions).transvect)
        retads = []
        for solution in solutions[-3:]:
            ads = adsorbate.copy()
            ads.rotate(solution.rotation,np.array([0,0,1]))
            ads.translate(np.array([solution.transvect[0][0],solution.transvect[0][1],0] ) )
            retads.append(ads)
        retlist = []
        for ads in retads:
            #print("t")
            slab = slab.copy()
            returnatms = rotate_and_translate(ads,slab,np.array([0,0,bondlength+1]),np.array([0,0,0]),rotcenter='COP')
            retlist.append(returnatms)
        return retlist
    else:
        #am I a line or a point?
        if len(Xpos)==2:
            #rotate adsorbate to lie on x axis
            adsorbate.rotate(Xpos[0]-Xpos[1],np.array([1,0,0]),center="COP")
            
            adslen = np.linalg.norm(Xpos[0]-Xpos[1])
            class Solution:
                rotation = 0
                transvect = np.array([0.0,0.0])
                intersectArea = 0 #nb: this actually checks if the intersectpolygon is much bigger than
            #the adsorbate polygon; if it is, it will set this number to -1
                def __lt__(self,other):
                    return self.intersectArea < other.intersectArea

            """Im a line, just look for lines similar in length to me!"""
            testSlab = slabPrimitive*(2,2,1) 
            #a 2x2 square will suffice to get the possible shapes without being too expensive
            #There will be a max of 4 identical shapes for n=3 or n=4 and 2 for n>4
            
            candidateAdsorbSites = [Be.position[:2] for Be in testSlab if Be.symbol=="Be"]
            unsats={0: [(0,0)], 1:candidateAdsorbSites,2:[],3:[],4:[]}
            #Get all possible n-gons
            #computes all nChooseN combinations internally so this is slow. Probably fix this later...
            
            for key in specialSymbols.keys():
                unsatSites = [Be.position[:2] for Be in testSlab if Be.symbol==key]
                unsats.update({specialSymbols[key]:unsatSites})
            sites = []
            #check what kind of lines we should consider
            mixed = False
            print(unsatClassCount)
            for key in unsatClassCount:
                if unsatClassCount[key]==2:
                    #they're both this type
                    sites = unsats[key]
                    print("unmixed")
                if unsatClassCount[key]==1:
                    print("mixed")
                    #they're mixed; make sure to have both type of adsorption sites
                    sites += unsats[key]
                    mixed = True
            print(unsatClassCount,sites)
            lines = list(itertools.permutations(sites,2))
            uniqueLines = removeDifferingByOrderOnly(lines)

            #move all the polygons to the center
            solutions = []
            bodge = np.array([1.711, 1.322]) #bug in the code for where the centroid is due to a slab offset??
            for polypoints in uniqueLines:
                solution = Solution()
                polypoints = list(polypoints)
                #print(polypoints)
                asline = np.array(polypoints[0]) - np.array(polypoints[1])
                linelen = np.linalg.norm(asline[1]-asline[0])
                centroid = (np.array(polypoints[0])+np.array(polypoints[1]))/2.0
                solution.transvect = centroid-bodge
                #print(asline)
                solution.angle=angle(asline)
                #check that the angle is correct: important if its a mixed line
                if mixed:
                    for x in xlist:
                        if x.unsaturation == 1:
                            #hinge point
                            #print(candidateAdsorbSites,np.array(polypoints[1]))
                            toTuples = [tuple(x) for x in candidateAdsorbSites]
                            if (tuple(polypoints[1]) in toTuples):
                                solution.angle=-angle(asline) #flip around
                solution.intersectArea = adslen - abs(linelen-adslen)
                solutions.append(solution)
            
            
            solutions.sort()
            bestsoln = max(solutions)
            retads = []
            for solution in solutions[-3:]:
                ads = adsorbate.copy()
                ads.rotate(solution.rotation,np.array([0,0,1]))
                #print(solution.transvect,bestsoln)
                ads.translate(np.array([solution.transvect[0],solution.transvect[1],0] ) )
                retads.append(ads)
            retlist = []
            for ads in retads:
                returnatms = rotate_and_translate(ads,slab,np.array([0,0,bondlength+1]),np.array([0,0,0]),rotcenter='COP')
                retlist.append(returnatms)
            return retlist
                #move to [0,0]
        if len(Xpos)==1:
            unsats={1:[],2:[],3:[],4:[]}
            tags = [atom for atom in adsorbate if atom.symbol=="X"]
            #move X to 0,0,0
            XPos = tags[0].position
            adsorbate.translate([-XPos[0],-XPos[1],-XPos[2]])
            print("I have only 1 X and my X pos is:",tags[0].position)
            """I'm a point, just find the 3 posible adsorb points (Ga, N, and trough)"""
            testSlab = slabPrimitive*(1,1,1)
            candidateSites = [Be.position for Be in testSlab if Be.symbol=="Be"]
            for key in specialSymbols.keys():
                degUnsat = specialSymbols[key]
                unsats.update({degUnsat:[Be.position for Be in testSlab if Be.symbol==key]})
            carb = getNeighbourAtoms(adsorbate,tags[0].index) [0]
            myUnsat = carb.index
            myUnsat = adsorbate.graph.nodes[carb.index]['unsaturation']
            solutions = []
            bodge = np.array([1.711, 1.322,0])
            if myUnsat==1:
                for site in candidateSites:
                    #site = site + np.array([0.64, 0.31,0])
                    ads = adsorbate.copy()
                    ads.translate(np.array([site[0],site[1] ,0] ) )
                    solutions.append(ads)
            else:
                for site in unsats[myUnsat]:
                    #site = site+np.array([0.64, 0.31,0])
                    ads = adsorbate.copy()
                    ads.translate(np.array([site[0],site[1] ,0] ) )
                    solutions.append(ads)          
    #rotate_and_translate(adsorbate,slab,translate,rotate,rotcenter)
        retlist =[]
        for solution in solutions:
            returnatms = rotate_and_translate(solution,slab,np.array([0,0,1.8]),np.array([0,0,0]),rotcenter='COP')
            retlist.append(returnatms)
        return retlist

def generateAdsorbs(adsorbate,slab,slabAnchorAtmSymbol,specialSymbols=[],bondlength=1.8):
    """
    adsorbate: a tagged (using generateAnagramsWithAnchorPoints) small molecule
    slab: a slab item made with the bulk() module, and SlabGenerator from the bulk, and slab = Slabgen.get_slab(). The surface atoms should be
    set using set_surface_atoms(), manually if need be (use view(slab)) to see which atoms you want
    slabAnchorAtmSymbol: By default the program looks for every adsorption site. This is to identify which atoms specifically can bind.
    returns a nice slab with adsorbate ready to plug into your favourite QM software.
    NB: Kinda buggy for having more than 1 adsorbate site.
    
    TODO: how to make it return a list of potential adsorbed positions??
    """
    numX = adsorbate.symbols.count('X')
    slab = slab.copy()
    retslab = slab.copy()*(6,4,1)
    slabSites = AdsorptionSites(slab)
    slabSymbols = slab.symbols.species()
    connectorAtm = slabSites.get_topology()
    adsites = slabSites.get_coordinates()
    connectivity = slabSites.get_connectivity()
    slabHeight = max([i.position[2] for i in slab if i.symbol in slabAnchorAtmSymbol])
    #What about Cl?
    slabBeIdx = [] #these contain the locations of the Be (representing C click points)
    slabFIdx = [] #these represent the F (representing the X click points) indexes
    for idx, site in enumerate(adsites):
        if connectivity[idx]==1:
            if slab[connectorAtm[idx][0]].symbol in slabAnchorAtmSymbol and slab[connectorAtm[idx][0]].position[2]==slabHeight:
                slab.append("Be")
                slab[len(slab)-1].position = site + np.array([0,0,0])
                slab.append("F")
                slab[len(slab)-1].position = site + np.array([0,0,bondlength])  
                
    #special sites were tagged in specialSymbols
    if len(specialSymbols)>0:
        for atom in slab:
            #replace the symbol with Be, and add F above it.
            if atom.symbol in specialSymbols:
                slab[atom.index].symbol ="Be"
                slab.append(Atom(symbol="F",position=atom.position+np.array([0,0,bondlength])))
                
    slab = slab*(6,4,1) #slab may beed to be thicker... [TODO]
    # Considering N choose NX
    for atm in slab:
        if atm.symbol == "Be":
            slabBeIdx.append(atm.index)
        if atm.symbol == "F":
            slabFIdx.append(atm.index)
    slabAnchorIndexHelper = list(range(len(slabFIdx)))
    slabConfigurations = []
    slabConfigurationScores = []
    """for removeConfiguration in itertools.combinations(slabAnchorIndexHelper,numX):
        sl = slab.copy()
        rmBe = []
        for i in removeConfiguration:
            rmBe.append(slabBeIdx[i])
        for atm in sl:
            if atm.symbol=="F":
                atm.symbol = "Ge" #get rid of the F for the time being
        for atm in sl:
            if atm.symbol=="Be":
                #print(removeConfiguration)
                if atm.index not in rmBe:
                    atm.symbol="Ge" #dummy Ge is not going to interact with anything
                else:
                    #print("ok")
                    sl.append("F")
                    sl[len(sl)-1].position = atm.position + np.array([0,0,bondlength]) #just to make sure the Be is associated with the F
                    
        slabConfigurations.append(sl)
    for sl in slabConfigurations:
        atms = rotate_and_translate(adsorbate,sl,np.array([0,0,1]),np.array([0,0,0]),rotcenter="COP")
        print(atms)
        slabConfigurationScores.append(lloss(atms,slabSymbols,bondlength=bondlength)) #unfortunately without rotating or 
        #or motion, this may infact be a less than optimal solution!
        #he alternative, is to measure the distances between the Be's that each pairwise distance matches roughly 
        #the pairwise distances of the X's of the adsorbate.
        
    slab = slabConfigurations[slabConfigurationScores.index(min(slabConfigurationScores) ) ]"""
    # This entire section may be undesirable
    
    atms = rotate_and_translate(adsorbate,slab,np.array([0,0,0]),np.array([0,bondlength,0]),rotcenter='COP')
    rotsol = np.array([0,0,0])
    transsol = np.array([0,0,0])
    """def toMinimizeT(translate):
        translate = translate
        atms = rotate_and_translate(adsorbate,slab,translate,rotsol)
        return loss(atms,slabSymbols)
    def toMinimizeR(rotate):
        rotate = rotate
        atms = rotate_and_translate(adsorbate,slab,transsol,rotate)
        return loss(atms,slabSymbols)"""
    def toMinimize(rotandtrans):
        #print(rotandtrans)
        translate = rotandtrans[:3]
        rotate = rotandtrans[3:]
        atms = rotate_and_translate(adsorbate,slab,translate,rotate,rotcenter="COP")
        #jac = np.zeros(6)
        coords1 = rotandtrans.copy()
        coords2 = rotandtrans.copy()
        epsilon = 1e-8 #sqrt(machine epsilon) is a good choice according to the handbook of numerical recipes
        """for x in range(6):
            coords1[x] = rotandtrans[x]+epsilon
            ttrans1= coords1[:3]
            trot1 = coords1[3:]
            coords2[x] = rotandtrans[x]-epsilon
            ttrans2= coords2[:3]
            trot2 = coords2[3:]

            testatms1 = rotate_and_translate(adsorbate,slab,ttrans1,trot1,rotcenter="COP")
            testatms2 = rotate_and_translate(adsorbate,slab,ttrans2,trot2,rotcenter="COP")

            l1 = lloss(testatms1,slabSymbols,bondlength=bondlength)
            l2 = lloss(testatms2,slabSymbols,bondlength=bondlength)
            jac[x] = (l1 - l2)/(2*epsilon)""" #testing the jacobian - speeds up computing slightly
        print("out",lloss(atms,slabSymbols,bondlength=bondlength))
        return lloss(atms,slabSymbols,bondlength=bondlength)#, jac
    """transsol = minimize(toMinimizeT,x0=np.array([0,0,0]),
                        bounds=(
                            (-3,3),
                            (-3,3),
                            (-0,0.1)
                            )
                        ).x"""
    minimizer_kwargs = {"method":"BFGS","tol":1e-1} #setting convergence tolerance to 1e-3
    import threading
    """Instead, as a consistency check, the algorithm can be run from a number of different random starting points 
    to ensure the lowest minimum found in each example has converged to the global minimum. 
    For this reason, basinhopping will by default simply run for the number of iterations niter 
    and return the lowest minimum found. It is left to the user to ensure that this is in fact the global minimum.
    
    Therefore, I will run this in (4?) threads, with different guesses for the rotation along z. Then I will choose the best one 
    to return.
    """
    allsolns = []
    initGuesses = [np.array([0,0,1,0,0,0]),np.array([0,0,1,0,0,90]),np.array([0,0,1,0,0,270])]
    threads = []
    def find_solutions(a,b,c,d,e,f):
        initial_guess = np.array([a,b,c,d,e,f])
        solution = basinhopping(toMinimize,x0=initial_guess,T=30,niter=50,niter_success=1,minimizer_kwargs=minimizer_kwargs,seed=1)
        allsolns.append(solution)
    for i in enumerate(initGuesses):
        #print(i[1])
        x = threading.Thread(target=find_solutions,args=i[1])
        x.start()
        threads.append(x)
    for x in threads:
        x.join()
        #print("x is joined")    
    candidateScores = []
    for solutions in allsolns:
        candidateScores.append(solutions.fun)
    
    #right now we are printing the best solution, but we can also have it return the best N solutions.
    #for solution in allsolns:
    solution = allsolns[candidateScores.index(min(candidateScores))].x
    print(solution)
    transsol = solution[:3]
    rotsol = solution[3:]
    """
    rotsol = minimize(toMinimizeR,x0=np.array([0,0,0]),method="L-BFGS-B",bounds=
                      (
                          (-90,90),
                          (-90,90),
                          (-90,90)
                          )
                      ).x #breaking up the two problems actually makes it a lot nicer """
    atms = rotate_and_translate(adsorbate,retslab,transsol,rotsol,rotcenter="COP")
    return atms#, slab
    #once minimized, add an edge between all the connecting points and delete the placeholder atoms.
def generateAdsorbsInOrder(adsorbate,slab,slabAnchorAtmSymbol):
    """

    Parameters
    ----------
    adsorbate : Gratoms
        A gratoms object with the desired adsorbate vector tagged by the fake element X
    slab : gratoms
        A gratoms object created by slabgenerator's get_slab() method.
    slabAnchorAtmSymbol : list
        A list of elements on slab that can be adsorbed to, ie, if you have a GaN slab, and only want to adsorb to Ga,
        you would pass ['Ga']

    Returns
    -------
    Gratoms
        A gratoms object of the adsorbate on a good guess position on the slab.

    """
    """this function will try to place the adsorbate correctly. It will try to place the adsorbate such that
    one anchor point is well placed. It will then attempt to rotate such that the next anchor point is well placed, and continue
    """
    adsorbate.center()
    slab = slab.copy()
    retslab = slab.copy()*(4,4,1)
    slabSites = AdsorptionSites(slab)
    slabSymbols = slab.symbols.species()
    connectorAtm = slabSites.get_topology()
    adsites = slabSites.get_coordinates()
    connectivity = slabSites.get_connectivity()
    for idx, site in enumerate(adsites):
        if connectivity[idx]==1:
            if slab[connectorAtm[idx][0]].symbol in slabAnchorAtmSymbol:
                slab.append("Ra")
                slab[len(slab)-1].position = site + np.array([0,0,0])
    slab = slab*(4,4,1)
    slabCenter = np.array([slab.cell[0]/2,slab.cell[1]/2,slab.cell[2]/2]).dot((np.array([1,1,1]))) + np.array([0,0,5])
    anchors = []
    Rasites = []
    for atom in adsorbate:
        if atom.symbol=="X":
            anchors.append(atom.index)
    for atom in slab:
        if atom.symbol=="Ra":
            Rasites.append(atom.position)
    #first atom is free to place as I please...
    #the closest slab anchor point in the middle shall be found...
    adsvec = adsorbate[anchors[0]].position
    target = np.array([0,0,0])
    for consite in Rasites:
        #print(consite,slabCenter,norm(consite-slabCenter))
        if norm(consite - slabCenter)<1.5:
            target = consite - slabCenter
            break
    transpos = target - adsvec #lock the adsorbate X to the anchor point
    #atms = rotate_and_translate(adsorbate,slab,transpos,np.array([0,0,0]),rotcenter=adsvec) #
    #print("adsvec, target,trpos ",adsvec,target,transpos) #by this stage, the first X is exactly on the desired anchor point
    #we can get rid of it, and rotate the molecule around until the second, third, etc, are in good positions, and the 
    #molecule isn't clipping through the surface.
    #lift transpos by 1angstrom so that rotating is easier for the minimizer
    
    transpos = transpos+np.array([0,0,0])
    """
    change this algorithm and actually search for the best rotation? We only have 360^3 to search which is not
    that big! 
    """
    coarseRotations = [i for i in range(3)] #1/10th 
    minrot = (1e90,(0,0,0))
    print('searching rotation space')
    for rotpos in itertools.product(coarseRotations,coarseRotations,coarseRotations):
        rotpos = np.array(rotpos)*(360/3)
        atms = rotate_and_translate(adsorbate,slab,transpos,rotpos,rotcenter=adsvec)
        cost = loss(atms,slabSymbols)
        print(rotpos)
        if cost < minrot[0]:
            minrot = (cost,rotpos)
            print(cost)
    """
    def toMinimizeR(rotate):
        rotate = rotate
        atms = rotate_and_translate(adsorbate,slab,transpos,rotate,rotcenter=adsvec)
        return loss(atms,slabSymbols)
    rotsol = np.array([0,0,0])
    
    rotsol = minimize(toMinimizeR,x0=np.array([0,0,0]),method="L-BFGS-B",bounds=
                      (
                          (-90,90),
                          (-90,90),
                          (-90,90)
                          )
                      ).x #breaking up the two problems actually makes it a lot nicer """
    adsorbate = adsorbate.copy()
    #adsorbate[anchors[0]].symbol="Li" #just to make it clearer for me...
    
    return rotate_and_translate(adsorbate,retslab,transpos,minrot[1],rotcenter=adsvec)
#todo: try to do the anchors one at a time. first anchor is moved and rotated to be good, then the molecule
#is rotated about the first anchor point until the rest are optimal...
def find_all_cycles(G, source=None, cycle_length_limit=None):
    #https://gist.github.com/joe-jordan/6548029
    #modification to line 9 to fix a bug with "not in list". 
    """forked from networkx dfs_edges function. Assumes nodes are integers, or at least
    types which work with min() and > .
    
    returns a list of cycles, with the nodes in them
    """
    if source is None:
        # produce edges for all components
        nodes=[list(i)[0] for i in nx.connected_components(G)]
    else:
        # produce edges for components with source
        nodes=[source]
    # extra variables for cycle detection:
    cycle_stack = []
    output_cycles = set()
    
    def get_hashable_cycle(cycle):
        """cycle as a tuple in a deterministic order."""
        m = min(cycle)
        mi = cycle.index(m)
        mi_plus_1 = mi + 1 if mi < len(cycle) - 1 else 0
        if cycle[mi-1] > cycle[mi_plus_1]:
            result = cycle[mi:] + cycle[:mi]
        else:
            result = list(reversed(cycle[:mi_plus_1])) + list(reversed(cycle[mi_plus_1:]))
        return tuple(result)
    
    for start in nodes:
        if start in cycle_stack:
            continue
        cycle_stack.append(start)
        
        stack = [(start,iter(G[start]))]
        while stack:
            parent,children = stack[-1]
            try:
                child = next(children)
                
                if child not in cycle_stack:
                    cycle_stack.append(child)
                    stack.append((child,iter(G[child])))
                else:
                    i = cycle_stack.index(child)
                    if i < len(cycle_stack) - 2: 
                      output_cycles.add(get_hashable_cycle(cycle_stack[i:]))
                
            except StopIteration:
                stack.pop()
                cycle_stack.pop()
    
    return [list(i) for i in output_cycles]        
