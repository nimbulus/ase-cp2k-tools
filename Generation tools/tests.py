#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 22:03:21 2021

@author: randon
"""
import networkx as nx
from pysmiles import write_smiles,read_smiles, fill_valence #https://github.com/pckroon/pysmiles
from ase.visualize import view
import ase.io
import utils
from os import listdir
from os.path import isfile, join
import itertools
import numpy as np
#import ase
#from pybel import *
import pybel

mypath = "/home/randon/Documents/homework/2020/mcgill/rustam/software/graph-generator/3/"
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for file in files:
    xyz = ""
    with open(mypath+file,'r') as f:
        xyz = f.read()
    mymol = pybel.readstring("xyz",xyz)
    smi_rep = mymol.write(format="smi",opt={})[:-2] #get rid of the \t\n
    """graph_mol = read_smiles(smi_rep,explicit_hydrogen=True)
    #Remove every n hydrogens...
    hydrogen_nodes = []
    for i in list(graph_mol.nodes(data='element')): #[(nodeNum,element),...]
        if i[1]=='H':
            hydrogen_nodes.append(i[0])#the node in question...
    numToRemoveMax = int(np.floor(len(hydrogen_nodes)/2))""" #doing it from smiles string...
    #What if I can just remove the hydrogen 
    #straight from the xyz file???
    lines = xyz.split('\n')
    lines = lines[2:]
    hydrogen_nodes = []
    for line in lines:
        if len(line)>0:
            if line[0]=='H':
                hydrogen_nodes.append(lines.index(line))
    numToRemoveMax = int(np.floor(len(hydrogen_nodes)/2))
    if numToRemoveMax>1:
        for i in range(numToRemoveMax-1):
            remove = [g for g in itertools.combinations(hydrogen_nodes,i+1)]
            for choice in remove:
                newMolecule = []# we're gonna construct this molecule line by line using the old xyz file
                xyzfile = ""
                for line in lines:
                    if lines.index(line) in choice:
                        #this is a hydrogen to get rid of...
                        pass
                    else:
                        newMolecule.append(line)
                xyzfile = xyzfile+str(len(newMolecule)-1)+'\n'
                for newline in newMolecule:
                    xyzfile = xyzfile+'\n'+newline
                newMol = pybel.readstring("xyz",xyzfile)
                newname = newMol.write()[:-2] #\t\n at the end of this string for silly reasons..
                with open(mypath+newname+".xyz",'w') as f:
                    f.write(xyzfile)
                
                
        
