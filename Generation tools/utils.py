import networkx as nx
from ase.build import bulk, add_adsorbate, surface
from ase.constraints import FixAtoms, FixBondLengths
from ase.eos import calculate_eos
from ase.db import connect
from ase.io import read, write
from ase.visualize import view
import numpy as np

def find_all_cycles(G, source=None, cycle_length_limit=None):
    #https://gist.github.com/joe-jordan/6548029
    #modification to line 9 to fix a bug with "not in list". 
    """forked from networkx dfs_edges function. Assumes nodes are integers, or at least
    types which work with min() and > ."""
    if source is None:
        # produce edges for all components
        nodes=[list(i)[0] for i in nx.connected_components(G)]
    else:
        # produce edges for components with source
        nodes=[source]
    # extra variables for cycle detection:
    cycle_stack = []
    output_cycles = set()
    
    def get_hashable_cycle(cycle):
        """cycle as a tuple in a deterministic order."""
        m = min(cycle)
        mi = cycle.index(m)
        mi_plus_1 = mi + 1 if mi < len(cycle) - 1 else 0
        if cycle[mi-1] > cycle[mi_plus_1]:
            result = cycle[mi:] + cycle[:mi]
        else:
            result = list(reversed(cycle[:mi_plus_1])) + list(reversed(cycle[mi_plus_1:]))
        return tuple(result)
    
    for start in nodes:
        if start in cycle_stack:
            continue
        cycle_stack.append(start)
        
        stack = [(start,iter(G[start]))]
        while stack:
            parent,children = stack[-1]
            try:
                child = next(children)
                
                if child not in cycle_stack:
                    cycle_stack.append(child)
                    stack.append((child,iter(G[child])))
                else:
                    i = cycle_stack.index(child)
                    if i < len(cycle_stack) - 2: 
                      output_cycles.add(get_hashable_cycle(cycle_stack[i:]))
                
            except StopIteration:
                stack.pop()
                cycle_stack.pop()
    
    return [list(i) for i in output_cycles]        

def getUndersaturatedAtom(molecule,atomnum=6): #take a pybel atom and get
    index = {'index':-1,'undersaturation':-1}
    for atom in molecule:
        if atom.atomicnum==atomnum:
            if (atom.implicitvalence - atom.valence) > index['undersaturation']:
                index['index'] = atom.idx-1 #for some ridiculous reason, idx starts at 1, not 0!
                index['undersaturation'] = atom.implicitvalence - atom.valence
    return index