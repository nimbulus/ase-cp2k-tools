"""
this program will build a GaN surface, and place the molecule as defined by the xyz files
in each of the folders, in a way that prefers it to be flat. It will then run a classical
molecular dynamics to optimize the structure, and be ready for electronic structure methods 
hopefully. 
"""
from ase.build import bulk, add_adsorbate, surface
from ase.constraints import FixAtoms, FixBondLengths
from ase.eos import calculate_eos
from ase.db import connect
from ase.io import read, write
from ase.visualize import view
from ase import Atoms
import numpy as np
import utils
import itertools 
from os import listdir
from os.path import isfile, join
import pybel
from ase.build.attach import attach, attach_index
use_asap = True #much more efficient, and installed!
#https://wiki.fysik.dtu.dk/asap/Installation
from ase.calculators.kim.kim import KIM
from ase.optimize import FIRE # a numerical gradient descent optimizer
from ase.optimize.basin import BasinHopping
#the jellium equation of state 10.1103/PhysRevB.67.026103
# Do one more calculation at the minimu and write to database:
#atoms.cell *= (v / atoms.get_volume())**(1 / 3)
#atoms.get_potential_energy()
def place(atoms,ads,name,z=4,pos=(6.491,5.805),indic=0):
    #pos is chosen to be a gallium atom node!
    #add_adsorbate(atoms, ads, height=z, position=pos,mol_index=indic) #this sticks it right smack in the middle.


    #attach_index is a patched version of attach found in ase, that will try to 
    #attach the molecule to a position and then rotate it to make it a physically 
    #reasonable structure. A hack is needed to identify z from attach_adsorbate, z=41.369
    #the normal vector away from the slab was arranged to be the z direction, (0,0,1)
    #def attach_index(atoms1, atoms2, slabpos,indexatom2, distance, direction=(1, 0, 0),
    atoms = attach_index(atoms,ads,np.array([ pos[0],pos[1],41.369 ]),indic,1,direction=(0,0,1) ) 
    #attach_index  asks you to place which 
    fixed = list(range(len(atoms) - len(ads) ))
    adsorbateAtoms = [atom for atom in atoms][len(atoms)-len(ads):] #after add_adsorbate, ase is appending the atoms of the adosrbate
    #to the list that is the "atoms". This selects all the atoms that came from the adsorbate
    adsorbateIndices = list(range(len(atoms) ) )[len(atoms)-len(ads):]
    fixBondLens = [g for g in itertools.combinations(adsorbateIndices,2)] #all pairs in adsorbate
    atoms.constraints = [FixAtoms(indices=fixed),FixBondLengths(fixBondLens)]
    #fixes the lengths between pairs of atoms. List comprises all
    #pairs in adsorbate. 
    #atoms.calc = KIM("LJ_ElliottAkerson_2015_Universal__MO_959249795837_003")
    #opt = FIRE(atoms)
    #opt.run(10)
    #https://openkim.org/id/LJ_ElliottAkerson_2015_Universal__MO_959249795837_003
    #a cheap lennard-jones-esque model just to get things sitting where they should be.
    return atoms

go =False #this is just me doing testing in the program. 
if go==True:
    for num in [3,4,5,6]: #number of atoms
        mypath = "./adsorbates/"+str(num)+"/"
        adsorbpath = "./adsorbed/"+str(num)+"/"
        files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for file in files:
            with open(mypath+file,'r') as f: #each file is a isomer of a molecule of interest, including hydrogens removed...
                with open(mypath+file,'r') as d:
                    textstr = d.read()
                adsorbate = read(f,format="xyz") #ase closes the file for bizzare reasons...
                adsorbate_pybel = pybel.readstring('xyz',textstr)
                #try to place 
                bulkGaN = bulk('NGa', 'wurtzite', a=9.7367653356401487/3.0, c=5.186,)#https://sci-hub.se/10.1016/B0-08-043152-6/00027-9
                mplaneGaN = surface(lattice = bulkGaN, indices = (1,0,0), layers = 4, vacuum = 32.0)
                #mplane is the only site that matters...
                mplaneGaN = mplaneGaN*(4,4,1)
                indices = utils.getUndersaturatedAtom(adsorbate_pybel)
                #we want to attach the lowest saturated carbon to the gallium. 
                v = place(mplaneGaN,adsorbate,adsorbpath+file+"-adsorb.xyz",z=2,indic=indices['index'])
                v.write(adsorbpath+file+'-adsorb.xyz',format='xyz')
#We may also wish to consider what happens when we detach a hydrogen as apparently occurs at the first
#step in MDA. A hydrogen will be abstracted from the desaturated carbon, and then attach a hydrogen to a nearby nitrogen
if go==True:
    for num in [3,4,5,6]: #number of atoms
        mypath = "./adsorbates/"+str(num)+"/"
        adsorbpath = "./adsorbed-hydrogen-removed/"+str(num)+"/"
        files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for file in files:
            with open(mypath+file,'r') as f: #each file is a isomer of a molecule of interest, including hydrogens removed...
                with open(mypath+file,'r') as d:
                    textstr = d.read()
                adsorbate = read(f,format="xyz") #ase closes the file for bizzare reasons...
                adsorbate_pybel = pybel.readstring('xyz',textstr)
                #try to place 
                bulkGaN = bulk('NGa', 'wurtzite', a=9.7367653356401487/3.0, c=5.186,)#https://sci-hub.se/10.1016/B0-08-043152-6/00027-9
                mplaneGaN = surface(lattice = bulkGaN, indices = (1,0,0), layers = 4, vacuum = 32.0)
                #mplane is the only site that matters...
                mplaneGaN = mplaneGaN*(4,4,1)
                #nitrogen next to the gallium center we will be adsorbing to is at 
                #6.491,7.779,41.369. Therefore we must also place a hydrogen there.
                indices = utils.getUndersaturatedAtom(adsorbate_pybel)
                #we want to attach the lowest saturated carbon to the gallium. 
                v = place(mplaneGaN,adsorbate,adsorbpath+file+"-adsorb.xyz",z=2,indic=indices['index'])
                v.write(adsorbpath+file+'-adsorb.xyz',format='xyz')
