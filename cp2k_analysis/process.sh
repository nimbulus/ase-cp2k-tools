#!/bin/bash

grep "E =" *.xyz > energies.txt
grep "Ideal and" *.out-001 > spins.txt

echo "set term png
set output 'energies.png'
set style line 1 lt 1 lw 2 pt 5  lc rgb 'red'
set style line 2 lt 1 lw 2 pt 7  lc rgb 'blue'
set style line 3 lt 1 lw 2 pt 9  lc rgb 'green'
set style line 4 lt 4 lw 2 pt 11  lc rgb 'black'
set style line 5 lt 5 lw 2 pt 13  lc rgb 'magenta'
set style line 6 lt 1 lw 2 pt 15  lc rgb 'dark-red'
set style line 7 lt 1 lw 2 pt 17  lc rgb 'dark-blue'
set style line 8 lt 1 lw 2 pt 19  lc rgb 'dark-green'
plot 'energies.txt' using 6 linestyle 1 title \"Energy (Ha) vs geom opt iteration\"
set output 'spins.png'
plot 'spins.txt' using 8 linestyle 1 title \"S**2\", \
 'spins.txt' using 7 linestyle 2 title \"Ideal S**2\"
" > qp.gp

gnuplot qp.gp
