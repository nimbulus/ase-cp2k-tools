#!/bin/sh
#SBATCH --account=ctb-rzk
#SBATCH --time=82:0:0
#SBATCH --mem-per-cpu=4G
#SBATCH --ntasks=32
#SBATCH --nodes=1
#SBATCH --job-name="CH2-dehydrogenation-NEB"
#SBATCH --cpus-per-task=2
#export SLURM_TMPDIR="/tmp/randon"
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
module load python/3.8
module load cp2k/8.2
echo $SLURM_TMPDIR
virtualenv --no-download $SLURM_TMPDIR/env
source $SLURM_TMPDIR/env/bin/activate
pip install --no-index --upgrade pip
pip install --no-index ase
pip install numpy --no-index
pip install pint --no-index
pip install regex --no-index
echo "starting driver from makevenv.sh"
python3 driver.py
