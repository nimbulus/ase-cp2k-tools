#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 14:31:30 2021

@author: randon
"""

from ase import io
import os
from ase.neb import NEB, DyNEB
from ase.optimize import BFGS, MDMin, FIRE
from ase.visualize import view
import numpy as np
from CP2KD import CP2K, CP2KSECTIONS
NIMAGES = 9 
helpers = CP2KSECTIONS()

if os.path.isfile(os.getcwd()+"/A2B.traj"):
    images = io.read(os.getcwd()+"/A2B.traj@-0:")
    images = images[len(images)-NIMAGES:]
    print("traj file found, restarting!",len(images),"images in restart file")
    neb = NEB(images, climb=True,)
else:
    initial = io.read('start.xyz')
    final = io.read('end.xyz')
    images = [initial]
    images += [initial.copy() for i in range(NIMAGES-2)]
    images += [final]
    neb = NEB(images, climb=False,) 
    """for the first few runs, turn off CI-NEB, as  CI-NEB
    depends on the adjacent images providing a reasonably good approximation
    of the correct tangent at the location of the climbing image; 
    thus in general the climbing image is not turned on until some iterations have been run 
    without it (generally 20% to 50% of the total number of iterations)."""
    neb.interpolate()

calcs = []
for image in images:
    image.cell = np.array([19.473530671280287,20.744,30.0])
    image.center()
    image.pbc = np.array([True,True,True])
    calc = CP2K(atoms=image,
                kinds=[helpers.PBE_kinds['H'],
                       helpers.PBE_kinds['Cl'],
                       helpers.PBE_kinds['C'],
                       helpers.PBE_kinds['Ga'],
                       helpers.PBE_kinds['N'],],
                multiplicity="1",binary=['cp2k'],
                charge="0"#spin of 0 - should be paired!
                          )
    calcs.append(calc)
    image.set_calculator(calc)
    print(calc._default_parameters['calc_id'],calc._id)
optimizer = FIRE(neb, trajectory='A2B.traj') #mdmin is an optimizer that uses
#use Newtonian dynamics with added friction, to converge to an energy minimum. It is apparently more robust for CI-NEB
#optimizer.run(fmax=0.04)
#images[0].get_total_energy()
