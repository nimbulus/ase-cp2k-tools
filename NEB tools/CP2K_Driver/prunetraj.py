from ase import io
from ase.calculators.calculator import PropertyNotImplementedError

traj = io.Trajectory('A2B.traj')
ntraj = io.trajectory.TrajectoryWriter('A2Bf.traj',mode='w')
for image in traj:
    try:
        print(image.get_total_energy())
        ntraj.write(image)
    except PropertyNotImplementedError as E:
        print("no calc")